from webbot import Browser
from array import *
import numpy as np
import datetime
import os.path
import time
import os

sessionScanCount=0

def minutesElapsed():
       now = datetime.datetime.now()
       return now.hour*60+now.minute


print("Welcome to moodlelook-client")
print("ANALYSE AT YOUR OWN RISK!")
print("Trying to get logged in...")
web = Browser(showWindow=False)
print("...found Browserdriver")
web.go_to('mywebpage')
web.type('myusername' , into='username' , id='username')
web.type('mypassword' , into='password' , id='password')
web.press(web.Key.ENTER)

print("Signed in")
html = web.get_page_source()


interval=1;
lastTime=minutesElapsed()
while True:
    now = datetime.datetime.now()
    print("Wait for next round... (Interval: ", interval, ")")
    while minutesElapsed() < lastTime + interval:
        time.sleep(1)

    print("/// Scan start ///")
    lastTime=minutesElapsed()
    sessionScanCount+=1

    currentDayTime = datetime.datetime.now()
    print ("Time of start: ", str(currentDayTime))

    web.go_to("mywebpage")
    html = web.get_page_source()

    newUsers=0
    existingUsers=0


    files = os.listdir("myoutputfolder")
    for fileName in files:
        with open("data/" + str(fileName), 'a') as file:
            file.write("\n0")


    endPoint=html.find("""Personen online (in den letzten 5 Minuten)""")-3

    i = 0
    while True:
        i += 1
        startPoint=html.find("""width="16" height="16" />""", endPoint)
        startPoint += 25
        endPoint=html.find("""</a></div><div class=""", startPoint)

        name = html[startPoint:endPoint]

        if len(name) >= 35:
            break;
        else:

            if os.path.isfile("data/" + str(name)): #Gibts schon
                lines = open("data/" +str(name)).read().splitlines()
                lines[len(lines)-1] = "1"
                open("data/" +str(name),'w').write('\n'.join(lines))
                existingUsers+=1
                print("-->", name)
            else: # Gibts noch nicht
                text_file = open("data/" + str(name) , "w")
                now = datetime.datetime.now()
                n=0
                while n < sessionScanCount-1:
                    text_file.write("0\n")
                    n=n+1
                text_file.write("1")
                text_file.close()
                newUsers+=1
                print("+++>", name)

    print("/// Scan is over ///")
    print("Existing users: ", existingUsers)
    print("New users:", newUsers)
    print("Total earnings: ", newUsers+existingUsers)
    print("Scan #", sessionScanCount)
    currentDayTime = datetime.datetime.now()
    print ("Time of end: ", str(currentDayTime))
