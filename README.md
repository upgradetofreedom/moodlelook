# Moodlelook
Written in python and R

Example for getting data with webbot, parsing it in python and drawing graphs with R. 
The example scans the online status of users at a moodle-instance by using the "Persons online" list.
